FROM 08101996/base_model_api:2.0.1
COPY requirements.txt /tmp
RUN pip install --no-cache-dir -r /tmp/requirements.txt
COPY . /code
WORKDIR /code
CMD mlchain run --port $PORT
